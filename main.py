import subprocess
import os
import numpy as np
import csv

#set the directory of the file to our current directory
BASE_DIR = os.path.dirname(os.path.abspath(__file__))

def calculateParametric():
    """Calculates parametric transformation to be used on Evan's monitor"""
    pixelCoordinatesx = np.array([[960], [960], [960]])
    pixelCoordinatesy = np.array([[200], [540], [880]])
    matrixA = np.array([[29.8, 6.2, 1], [29.8, 16.75, 1], [29.8, 27.3, 1]]) # equation 3, units in cm
    matrixAinv = np.linalg.pinv(matrixA)
    xcoeff = np.array(np.matmul(matrixAinv, pixelCoordinatesx)) # equation 5
    ycoeff = np.array(np.matmul(matrixAinv, pixelCoordinatesy))
    coeffs = np.array([xcoeff, ycoeff])
    return (coeffs)


def applyTransformation(coeffs):
    temp = [0, 0]
    newcoord = list()
    z = 0
    xcm = 0
    ycm = 0
    with open('recentOutput2.csv', 'r') as csv_file:
        csv_reader = csv.reader(csv_file)
        # print("Here is the first digit in the matrix :", coeffs[[0][0], [0]])
        # print("Here is the second digit in the matrix :", coeffs[[0][0], [1]])
        # print("Here is the fifth digit in the matrix :", coeffs[[1][0], [1]])
        # print(coeffs[[1][0]])
        # print(coeffs[[2][0]])
        for line in csv_reader:
            # print(line[3])

            # a = int(line[0])
            # print(line[7])
            # if line[3] < .80:
            # continue
            # print(line[3])
            leftAvgZ = 0
            rightAvgZ = 0
            for q in range(257, 265, 1):
                leftAvgZ = (leftAvgZ + float(line[q]))/10
            leftAvgZ/8
            for q in range(285, 293, 1):
                rightAvgZ = (rightAvgZ + float(line[q]))/10
            rightAvgZ/8
            print (leftAvgZ)
            print(rightAvgZ)
            z = leftAvgZ+rightAvgZ/2
            # print(z)
            # print(line[11])
            # print(line[12])
            xcm = -1 * z * np.sin(float(line[11]))
            ycm = z * np.sin(float(line[12]))
            # print(xcm)
            # print(ycm)
            temp[0] = float(coeffs[[0][0], [0]]) * xcm + float(coeffs[[0][0], [1]]) * ycm + float(coeffs[[0][0], [2]])
            temp[1] = float(coeffs[[1][0], [0]]) * xcm + float(coeffs[[1][0], [1]]) * ycm + float(coeffs[[1][0], [2]])
            # print(temp)
    return (1)


os.system("eog --fullscreen ./target-template2.png &")
# os.system("/home/evan/Desktop/OpenFace/OpenFace/build/bin/./FeatureExtraction -device 2 -of recentOutput.csv -out_dir /home/evan/Desktop/EyeTrackingResearch/phaseOne/outputs")
coeffmatrix = calculateParametric()
print(applyTransformation(coeffmatrix))
# os.system("pkill FeatureExtraction")
